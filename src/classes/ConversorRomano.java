package classes;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import interfaces.IConversor;

public class ConversorRomano implements IConversor {
	
	final HashMap<Integer, String> ValoresBase = new HashMap<Integer, String>();
	
	final Integer[] valoresBase = { 1, 5, 10, 50, 100, 500, 1000};
	
	public ConversorRomano() {
		ValoresBase.put(new Integer(1), "I");
		//ValoresBase.put(new Integer(2), "II");
		//ValoresBase.put(new Integer(4), "IV");
		ValoresBase.put(new Integer(5), "V");
		ValoresBase.put(new Integer(10), "X");
		ValoresBase.put(new Integer(50), "L");
		ValoresBase.put(new Integer(100), "C");
		ValoresBase.put(new Integer(500), "D");
		ValoresBase.put(new Integer(1000), "M");
		
	}

	public String converter2(int numero) {
		
		String romano = ValoresBase.get(new Integer(numero));
	
		if(romano == null) {
			romano = "";
			for (int i = 0; i < numero; i++)
			{
				romano += ValoresBase.get(new Integer(1));
			}
		}

		return romano;
	}
	
	@Override
	public String converter(int numero) {
		String romano = "";
		
		// Integer keys[] = (Integer[]) ValoresBase.keySet().toArray();
		// keys = sort(Arrays.asList(keys));
		Integer[] keys = valoresBase;
		
		int tamanho = keys.length;
		while(numero > 0 && tamanho > 0) {
			while(numero >= keys[tamanho-1]) {
				romano = romano + ValoresBase.get(keys[tamanho-1]);
				numero -= keys[tamanho-1];
			}
			if((tamanho%2 == 1) && tamanho>1) {
				int aux = keys[tamanho-1]-keys[tamanho-3];
				if(numero>aux) {
					romano += ValoresBase.get(keys[tamanho-3]);
					romano += ValoresBase.get(keys[tamanho-1]);
					numero -= aux;
				}
			}else if(tamanho%2 == 0) {
				int aux = keys[tamanho-1]-keys[tamanho-2];
				if(numero>aux) {
					romano += ValoresBase.get(keys[tamanho-2]);
					romano += ValoresBase.get(keys[tamanho-1]);
					numero -=aux;
				}
			}
			tamanho--;
		}

		return romano;
	}
	
	private Integer[] sort(List<Integer> keys){
		Collections.sort(keys);
		return (Integer[]) keys.toArray();
	}
}
