import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import classes.ConversorRomano;
import interfaces.IConversor;

class TestRomanos {
	
	private static IConversor conversor;
	
	@BeforeAll
	static void setup() {
		conversor = new ConversorRomano();
	}
	
	@Test
	void verificar_se_1_equals_I() {
		int numero = 1;
		assertEquals("I", conversor.converter(numero));
	}
	
	@Test
	void verificar_se_2_equals_II() {
		int numero = 2;
		assertEquals("II", conversor.converter(numero));
	}
	
	@Test
	void verificar_se_4_equals_IV() {
		int numero = 4;
		assertEquals("IV", conversor.converter(numero));
	}
	
	@Test
	void verificar_se_5_equals_V() {
		int numero = 5;
		assertEquals("V", conversor.converter(numero));
	}
	
	@Test
	void verificar_se_10_equals_X() {
		int numero = 10;
		assertEquals("X", conversor.converter(numero));
	}
	
	@Test
	void verificar_se_50_equals_L() {
		int numero = 50;
		assertEquals("L", conversor.converter(numero));
	}
	
	@Test
	void verificar_se_100_equals_C() {
		int numero = 100;
		assertEquals("C", conversor.converter(numero));
	}
	
	@Test
	void verificar_se_500_equals_D() {
		int numero = 500;
		assertEquals("D", conversor.converter(numero));
	}
	
	@Test
	void verificar_se_1000_equals_M() {
		int numero = 1000;
		assertEquals("M", conversor.converter(numero));
	}

	@Test
	void verificar_se_3_equals_III() {
		int numero = 3;
		assertEquals("III", conversor.converter(numero));
	}

	@Test
	void verificar_se_6_equals_VI() {
		int numero = 6;
		assertEquals("VI", conversor.converter(numero));
	}
	
	
}
